﻿namespace ExternalAuth.WeixinOpen.Models
{
    /// <summary>
    /// 昵称、头像更新对象
    /// </summary>
    public class MoUpdate
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string nickName { get; set; }
        /// <summary>
        /// 头像Url
        /// </summary>
        public string avatarUrl { get; set; }
    }
}