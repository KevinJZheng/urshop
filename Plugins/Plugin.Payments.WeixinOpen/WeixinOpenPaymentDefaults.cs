﻿
namespace Urs.Plugin.Payments.WeixinOpen
{
    /// <summary>
    /// Represents constants of the Weixin payment plugin
    /// </summary>
    public static class WeixinOpenPaymentDefaults
    {
        public static string SystemName => "Payments.WeixinOpen";

        public const string ViewComponentName = "PaymentWeixinOpen";

        public const string PaymentWeixinOpenPayingUrl = "/Plugins/PaymentWeixinOpen/Paying";

        public const string PaymentPayUrl = "https://www.alipay.com/cooperate/gateway.do?_input_charset=utf-8";

        public const string PaymentMethod = "POST";
    }
}