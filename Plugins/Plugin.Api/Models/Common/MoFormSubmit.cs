﻿using System.Collections.Generic;
using Plugin.Api.Models.ShoppingCart;

namespace Plugin.Api.Models.Common
{
    /// <summary>
    /// 提交表单
    /// </summary>
    public class MoFormSubmit
    {
        public MoFormSubmit()
        {
            Form = new List<MoKeyValue>();
        }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 提交自定义字段
        /// </summary>
        public IList<MoKeyValue> Form { get; set; }
    }
}