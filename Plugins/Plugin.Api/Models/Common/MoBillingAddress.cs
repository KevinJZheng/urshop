﻿namespace Plugin.Api.Models.Common
{
    /// <summary>
    /// 发票
    /// </summary>
    public class MoBillingAddress
    {
        /// <summary>
        /// 发票Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool BillingEnabled { get; set; }
        /// <summary>
        /// 发票类型Id
        /// </summary>
        public int BillingTypeId { get; set; }
        /// <summary>
        /// 发票类型
        /// </summary>
        public string BillingType { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// 纳税人识别码
        /// </summary>
        public string TaxpayerId { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// 银行
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// 银行账户
        /// </summary>
        public string BankAccount { get; set; }
        /// <summary>
        /// 抬头Id
        /// </summary>
        public virtual int BillingTitleId { get; set; }
        /// <summary>
        /// 抬头
        /// </summary>
        public string BillingTitle { get; set; }
        /// <summary>
        /// 抬头名称
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 是否选择
        /// </summary>
        public bool Selected { get; set; }
    }
}