﻿using FluentValidation.Attributes;

namespace Plugin.Api.Models.Order
{
    /// <summary>
    /// 退换货
    /// </summary>
    [Validator(typeof(MoReturnValidator))]
    public partial class MoReturn
    {
        /// <summary>
        /// 订单商品项Id （二选一）
        /// </summary>
        public int OrderGoodsId { get; set; }
        /// <summary>
        /// 订单Id （二选一）
        /// </summary>
        public int OrderId { get; set; }
        /// <summary>
        /// 退货数量
        /// </summary>
        public int Qty { get; set; }
        /// <summary>
        /// 退货原因
        /// </summary>
        public string Reason { get; set; }
        /// <summary>
        /// 退货操作
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public string Comments { get; set; }
        /// <summary>
        /// 图片 图片Id用逗号隔开 如  124,2543
        /// </summary>
        public string Images { get; set; }

    }
}