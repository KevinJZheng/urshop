﻿using Urs.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Plugin.Api.Models.ShoppingCart
{
    /// <summary>
    /// 添加购物车
    /// </summary>
    public class MoAddCart
    {
        public MoAddCart()
        {
            Form = new List<MoKeyValue>();
        }
        /// <summary>
        /// 商品Id
        /// </summary>
        public int PId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Qty { get; set; }
        /// <summary>
        /// 多规格
        /// </summary>
        public string AttrValue { get; set; }
        /// <summary>
        /// 提交表单
        /// </summary>
        public IList<MoKeyValue> Form { get; set; }
    }
    /// <summary>
    /// 键值对  
    /// </summary>
    public partial class MoKeyValue
    {
        /// <summary>
        /// 键
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }
    }
}