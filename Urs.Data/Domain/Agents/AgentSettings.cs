﻿using Urs.Core.Configuration;
using Urs.Data.Domain.Orders;

namespace Urs.Data.Domain.Agents
{
    public class AgentSettings : ISettings
    {
        /// <summary>
        /// 启动
        /// </summary>
        public bool Enabled { get; set; }
        /// <summary>
        /// 分红一级佣金比率
        /// </summary>
        public decimal Rate { get; set; }
        /// <summary>
        /// 二级分销启用
        /// </summary>
        public bool ParentAgentEnabled { get; set; }
        /// <summary>
        /// 分红二级佣金比率
        /// </summary>
        public decimal ParentRate { get; set; }
        /// <summary>
        /// 佣金计算方式
        /// </summary>
        public AgentBonusMethod Method { get; set; }
        /// <summary>
        /// 佣金兑换状态
        /// </summary>
        public OrderStatus BonusForHandout_Awarded { get; set; }
        /// <summary>
        /// 取消状态
        /// </summary>
        public OrderStatus BonusForHandout_Canceled { get; set; }
    }


    public enum AgentBonusMethod : int
    {
        OrderSubtotal = 5,
        OrderTotal = 10,
        //ProductUnit = 15,
        ProductProfit = 20
    }
}