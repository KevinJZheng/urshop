using Urs.Core;
using System;
using Urs.Data.Domain.Users;

namespace Urs.Data.Domain.Orders
{
    public partial class AfterSales : BaseEntity
    {
        public virtual int OrderId { get; set; }
        /// <summary>
        /// 具体订单子项
        /// </summary>
        public virtual int OrderItemId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public virtual int UserId { get; set; }
        /// <summary>
        ///数量
        /// </summary>
        public virtual int Quantity { get; set; }
        /// <summary>
        /// 退换货原因
        /// </summary>
        public virtual string ReasonForReturn { get; set; }
        /// <summary>
        /// 售后事项
        /// </summary>
        public virtual string RequestedAction { get; set; }
        /// <summary>
        /// 用户回复
        /// </summary>
        public virtual string UserComments { get; set; }
        /// <summary>
        ///笔记
        /// </summary>
        public virtual string StaffNotes { get; set; }
        public virtual string Images { get; set; }
        /// <summary>
        /// 售后状态
        /// </summary>
        public virtual int AfterSalesStatusId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime CreateTime { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public virtual DateTime UpdateTime { get; set; }
        public virtual AfterSalesStatus AfterSalesStatus
        {
            get
            {
                return (AfterSalesStatus)this.AfterSalesStatusId;
            }
            set
            {
                this.AfterSalesStatusId = (int)value;
            }
        }
    }
}
