using Urs.Core;
namespace Urs.Data.Domain.Localization
{
    public partial class LocaleStringResource : BaseEntity
    {
        public virtual string Name { get; set; }

        public virtual string Value { get; set; }
    }
}
