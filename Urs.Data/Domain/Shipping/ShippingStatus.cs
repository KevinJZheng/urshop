namespace Urs.Data.Domain.Shipping
{
    /// <summary>
    /// ����ö��
    /// </summary>
    public enum ShippingStatus : int
    {
        /// <summary>
        /// ��������
        /// </summary>
        ShippingNotRequired = 10,
        /// <summary>
        /// δ����
        /// </summary>
        NotYetShipped = 20,
        /// <summary>
        /// �Ѳ�������
        /// </summary>
        PartiallyShipped = 25,
        /// <summary>
        /// ������
        /// </summary>
        Shipped = 30,
        /// <summary>
        /// ���ʹ�
        /// </summary>
        Delivered = 40,
    }
}
