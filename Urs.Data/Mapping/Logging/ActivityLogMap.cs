﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Logging;

namespace Urs.Data.Mapping.Logging
{
    public partial class ActivityLogMap : UrsEntityTypeConfiguration<ActivityLog>
    {
        public override void Configure(EntityTypeBuilder<ActivityLog> builder)
        {
            builder.ToTable(nameof(ActivityLog));
            builder.HasKey(al => al.Id);
            builder.Property(al => al.Comment).IsRequired();

            base.Configure(builder);
        }
    }
}
