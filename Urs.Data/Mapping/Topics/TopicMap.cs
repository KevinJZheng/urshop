﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Topics;

namespace Urs.Data.Mapping.Topics
{
    public class TopicMap : UrsEntityTypeConfiguration<Topic>
    {
        public override void Configure(EntityTypeBuilder<Topic> builder)
        {
            builder.ToTable(nameof(Topic));
            builder.HasKey(t => t.Id);
            builder.Property(t => t.SystemName);
            builder.Property(t => t.Short);
            builder.Property(t => t.Title);
            builder.Property(t => t.Body);
            base.Configure(builder);
        }
    }
}
