﻿using Urs.Data.Domain.Users;
using Urs.Data.Domain.Directory;
using Urs.Data.Domain.Localization;

namespace Urs.Core
{
    /// <summary>
    /// Work context
    /// </summary>
    public interface IWorkContext
    {
        /// <summary>
        /// Gets or sets the current user
        /// </summary>
        User CurrentUser { get; set; }
    }
}
