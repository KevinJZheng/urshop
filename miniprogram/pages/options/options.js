const app = getApp()
Page({

  data: {
    AdresList: [],
    historyList:[],
    home: false,
    searchList:[],
    searchPage: false,
    searclosebtn: false,
    currentAdres:''
  },
  onLoad: function (options) {
    if (options.home){
      this.setData({
        home: true
      })
    }
    let arr = app.globalData.location
    let adresList = []
    let length=arr!=null&&arr.length>10?10:arr.length
    for(let i=0;i<length;i++){
      adresList.push(arr[i])
    }
    this.setData({
      AdresList: adresList,
      currentAdres: app.globalData.currentAdress,
      historyList: wx.getStorageSync('historyAdress')
    })
  },
  onReady: function () {
    
  },
  switchClick:function(e){
    app.globalData.currentAdress = e.currentTarget.dataset.adres
    wx.setStorageSync('adress', app.globalData.currentAdress)
    if (this.data.home){
      wx.switchTab({
        url: '/pages/index/index',
      })
    }else{
      wx.navigateTo({
        url: '/pages/checkout/checkout',
      })
    }
  },
  deletHistory: function(){
    wx.removeStorageSync('historyAdress')
    this.setData({
      historyList:[]
    })
  },
  //搜索
  search: function (e) {
    let inputValue = e.detail.value
    let cityArr = app.globalData.location
    let arr = []
    for (let i = 0; i < cityArr.length;i++){
      if (cityArr[i].adres.Name.indexOf(inputValue)!= -1){
        arr.push(cityArr[i])
      }
    }
    this.setData({
      searchList: arr,
      searclosebtn: true,
      searchPage: true
    })
  },
  closeBtn:function(){
    this.setData({
      searchPage: false,
      searclosebtn: false
    })
  }
})