﻿using System.Collections.Generic;

namespace Urs.Services.Users 
{
    public class PasswordRequestResult
    {
        public IList<string> Errors { get; set; }

        public PasswordRequestResult() 
        {
            this.Errors = new List<string>();
        }

        public bool Success
        {
            get { return (this.Errors.Count == 0); }
        }

        public void AddError(string error) 
        {
            this.Errors.Add(error);
        }
    }
}
