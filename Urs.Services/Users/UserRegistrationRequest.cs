﻿using Urs.Core;
using Urs.Data.Domain.Users;

namespace Urs.Services.Users
{
    public class UserRegistrationRequest
    {
        public User User { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public PasswordFormat PasswordFormat { get; set; }
        public bool IsApproved { get; set; }
        public string AccountName { get; set; }
        public UserRegistrationType UserRegistrationType { get; set; }
        public UserRegistrationRequest() { }

        public UserRegistrationRequest(User user,
            string email,
            string username,
            string phone,
            string password,
            PasswordFormat passwordFormat,
            bool isApproved = true)
        {
            this.User = user;
            this.Email = email;
            this.Username = username;
            this.Phone = phone;
            this.Password = password;
            this.PasswordFormat = passwordFormat;
            this.IsApproved = isApproved;
        }

        public UserRegistrationType Init(User user, string account, string password, PasswordFormat passwordFormat, UserRegistrationType userRegistrtionType,
            bool isApproved = true)
        {
            this.User = user;
            this.Password = password;
            this.PasswordFormat = passwordFormat;
            this.IsApproved = isApproved;

            this.UserRegistrationType = UserRegistrationType.Disabled;
            if (CommonHelper.IsValidPhone(account))
            {
                this.Phone = account;
                this.AccountName = account;
                this.UserRegistrationType = UserRegistrationType.Phone;
            }

            if (userRegistrtionType == UserRegistrationType.AdminApproval)
            {
                this.UserRegistrationType = UserRegistrationType.AdminApproval;
            }
            else if (userRegistrtionType == UserRegistrationType.Disabled)
            {
                this.UserRegistrationType = UserRegistrationType.Disabled;
            }

            return this.UserRegistrationType;
        }
    }
}
