using Urs.Data.Domain.Users;

namespace Urs.Services.Users
{
    public partial interface IUserRegistrationService
    {
        bool Validate(string usernameOrEmailOrPhone, string password);

        UserRegistrationResult RegisterUser(UserRegistrationRequest request);

        PasswordRequestResult CheckPassword(CheckPasswordRequest request);

        PasswordRequestResult ChangePassword(ChangePasswordRequest request);

        void SetPhone(User user, string newPhone);
        
    }
}