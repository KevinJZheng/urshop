using System;
using Urs.Core;
using Urs.Data.Domain.Localization;
using Urs.Core.Infrastructure;
using Urs.Core.Plugins;

namespace Urs.Services.Localization
{
    public static class LocalizationExtentions
    {
        public static string GetLocalizedEnum<T>(this T enumValue, ILocalizationService localizationService)
            where T : struct
        {
            if (localizationService == null)
                throw new ArgumentNullException("localizationService");

            if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enumerated type");

            string resourceName = string.Format("Enums.{0}.{1}",
                typeof(T).ToString(),
                enumValue.ToString());
            string result = localizationService.GetResource(resourceName, false, "", true);

            if (String.IsNullOrEmpty(result))
                result = CommonHelper.ConvertEnum(enumValue.ToString());

            return result;
        }

        public static void DeletePluginLocaleResource(this BasePlugin plugin,
            string resourceName)
        {
            var localizationService = EngineContext.Current.Resolve<ILocalizationService>();
            DeletePluginLocaleResource(plugin, localizationService, resourceName);
        }
        public static void DeletePluginLocaleResource(this BasePlugin plugin,
            ILocalizationService localizationService, string resourceName)
        {
            if (plugin == null)
                throw new ArgumentNullException("plugin");
            if (localizationService == null)
                throw new ArgumentNullException("localizationService");

            var lsr = localizationService.GetLocaleStringResourceByName(resourceName, false);
            if (lsr != null)
                localizationService.DeleteLocaleStringResource(lsr);
        }
        public static void AddOrUpdatePluginLocaleResource(this BasePlugin plugin,
            string resourceName, string resourceValue)
        {
            var localizationService = EngineContext.Current.Resolve<ILocalizationService>();
            AddOrUpdatePluginLocaleResource(plugin, localizationService,
                 resourceName, resourceValue);
        }
        public static void AddOrUpdatePluginLocaleResource(this BasePlugin plugin,
            ILocalizationService localizationService,
            string key, string value)
        {
            if (plugin == null)
                throw new ArgumentNullException("plugin");
            if (localizationService == null)
                throw new ArgumentNullException("localizationService");

            var lsr = localizationService.GetLocaleStringResourceByName(key, false);
            if (lsr == null)
            {
                lsr = new LocaleStringResource()
                {
                    Name = key,
                    Value = value
                };
                localizationService.InsertLocaleStringResource(lsr);
            }
            else
            {
                lsr.Value = value;
                localizationService.UpdateLocaleStringResource(lsr);
            }
        }



        public static string GetLocalizedFriendlyName<T>(this T plugin, ILocalizationService localizationService,
            bool returnDefaultValue = true)
            where T : IPlugin
        {
            if (localizationService == null)
                throw new ArgumentNullException("localizationService");

            if (plugin == null)
                throw new ArgumentNullException("plugin");

            if (plugin.PluginDescriptor == null)
                throw new ArgumentException("Plugin descriptor cannot be loaded");

            string systemName = plugin.PluginDescriptor.SystemName;
            string resourceName = string.Format("Plugins.FriendlyName.{0}",
                systemName);
            string result = localizationService.GetResource(resourceName, false, "", true);

            if (String.IsNullOrEmpty(result) && returnDefaultValue)
                result = plugin.PluginDescriptor.FriendlyName;

            return result;
        }
    }
}
