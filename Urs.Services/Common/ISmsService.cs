﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urs.Services.Common
{
    public interface ISmsService
    {
        bool Send(string phoneNumber, string content);

        bool BatchSend(string[] phoneNumbers, string content);

        string Receive(string phoneNumber);
    }
}
