using System.Collections.Generic;
using Urs.Data.Domain.Stores;
using Urs.Core;

namespace Urs.Services.Stores
{
    public partial interface IGoodsParameterService
    {
        #region Parameter group

        GoodsParameterGroup GetParameterGroupById(int parameterGroupId);

        IList<GoodsParameterGroup> GetAllParameterGroups();

        IPagedList<GoodsParameterGroup> GetParameterGroups(int pageIndex, int pageSize);

        void DeleteParameterGroup(GoodsParameterGroup parameterGroup);

        void InsertParameterGroup(GoodsParameterGroup parameterGroup);

        void UpdateParameterGroup(GoodsParameterGroup parameterGroup);

        #endregion

        #region Goods parameter

        GoodsParameter GetGoodsParameterById(int goodsParameterId);

        IList<GoodsParameter> GetGoodsParameterList(string name = null, List<int> ids = null);

        IPagedList<GoodsParameter> GetGoodsParameters(string name = null, int groupId = 0, int pageIndex = 0, int pageSize = int.MaxValue);
        void DeleteGoodsParameter(GoodsParameter goodsParameter);

        void InsertGoodsParameter(GoodsParameter goodsParameter);

        void UpdateGoodsParameter(GoodsParameter goodsParameter);

        #endregion

        #region Goods parameter option

        GoodsParameterOption GetGoodsParameterOptionById(int goodsParameterOption);

        IList<GoodsParameterOption> GetGoodsParameterOptionsByGoodsParameter(int goodsParameterId);

        void DeleteGoodsParameterOption(GoodsParameterOption goodsParameterOption);

        void InsertGoodsParameterOption(GoodsParameterOption goodsParameterOption);

        void UpdateGoodsParameterOption(GoodsParameterOption goodsParameterOption);

        #endregion

        #region Goods parameter mapping

        void DeleteGoodsParameterMapping(GoodsParameterMapping goodsParameterMapping);

        IList<GoodsParameterMapping> GetGoodsParameterMappingByGoodsId(int goodsId);

        IList<GoodsParameterMapping> GetGoodsParameterMappingByGoodsId(int goodsId,
            bool? allowFiltering, bool? showOnGoodsPage);

        GoodsParameterMapping GetGoodsParameterMappingById(int goodsParameterMappingId);

        void InsertGoodsParameterMapping(GoodsParameterMapping goodsParameterMapping);

        void UpdateGoodsParameterMapping(GoodsParameterMapping goodsParameterMapping);

        #endregion
    }
}
