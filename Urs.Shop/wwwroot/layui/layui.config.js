layui.config({
    base: '/layui/' //静态资源所在路径
}).extend({
    index: "layui.index"
    , miniAdmin: "mini/miniAdmin" // layuimini后台扩展
    , miniMenu: "mini/miniMenu" // layuimini菜单扩展
    , miniTab: "mini/miniTab" // layuimini tab扩展
    , miniTheme: "mini/miniTheme" // layuimini 主题扩展
    , formSelects: 'formSelects-v4'
    , treeTable: 'treeTable'
    , cookie: 'cookie'
    , notice: 'notice'
    , soulTable: 'soulTable'
    , formTable: 'formTable'
    , echarts: 'echarts/echarts' // echarts图表扩展
    , echartsTheme: 'echarts/echartsTheme' // echarts图表主题扩展
});
layui.use(['jquery', 'layer', 'miniAdmin'], function () {
    var $ = layui.jquery,
        layer = layui.layer
        , miniTab = layui.miniTab
        , miniTheme = layui.miniTheme
        , miniMenu = layui.miniMenu
        ,miniAdmin = layui.miniAdmin;

    miniAdmin.listen();
    miniAdmin.renderAnim(true);
    miniAdmin.deleteLoader(0);
    miniMenu.listen({});
    miniTab.listen({});
    miniTheme.render({
        bgColorDefault: false,
        listen: true,
    });
    //miniAdmin.render(options);
});