﻿using FluentValidation;
using Urs.Admin.Models.Settings;
using Urs.Data.Domain.Orders;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Urs.Admin.Validators.Settings
{
    public class PointsSettingsValidator : BaseUrsValidator<PointsSettingsModel>
    {
        public PointsSettingsValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.PointsForPurchases_Awarded).NotEqual((int)OrderStatus.Pending).WithMessage(localizationService.GetResource("Admin.Configuration.Settings.RewardPoints.PointsForPurchases_Awarded.Pending"));
            RuleFor(x => x.PointsForPurchases_Canceled).NotEqual((int)OrderStatus.Pending).WithMessage(localizationService.GetResource("Admin.Configuration.Settings.RewardPoints.PointsForPurchases_Canceled.Pending"));
        }
    }
}