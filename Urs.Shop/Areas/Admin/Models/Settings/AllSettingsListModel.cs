﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Settings
{
    public class AllSettingsListModel : BaseModel
    {
        [UrsDisplayName("Admin.Configuration.Settings.AllSettings.SearchSettingName")]
        public string Name { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.AllSettings.SearchSettingValue")]
        public string Value { get; set; }
    }
}