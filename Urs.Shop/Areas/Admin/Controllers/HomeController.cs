﻿using Microsoft.AspNetCore.Mvc;
using Urs.Admin.Models.Home;
using Urs.Core;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Orders;
using Urs.Services.Stores;
using Urs.Services.Users;
using Urs.Services.Orders;
using Urs.Services.Security;
using Urs.Framework.Controllers;

namespace Urs.Admin.Controllers
{
    /// <summary>
    /// www.urselect.com 优社电商
    /// </summary>
    [AdminAuthorize]
    public partial class HomeController : BaseAdminController
    {
        #region Fields

        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly IOrderService _orderService;
        private readonly IUserService _userService;
        private readonly IGoodsService _goodsService;

        #endregion

        #region Ctor

        public HomeController(
            IPermissionService permissionService,
            IWorkContext workContext,
            IOrderService orderService,
            IUserService userService,
            IGoodsService goodsService)
        {
            this._permissionService = permissionService;
            this._workContext = workContext;
            this._orderService = orderService;
            this._userService = userService;
            this._goodsService = goodsService;
        }

        #endregion

        #region Methods

        public IActionResult Index()
        {


            return View();
        }

        public IActionResult Console()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers) ||
              !_permissionService.Authorize(StandardPermissionProvider.ManageOrders) ||
              !_permissionService.Authorize(StandardPermissionProvider.ManageAfterSales) ||
              !_permissionService.Authorize(StandardPermissionProvider.ManageCatalog))
                return Content("");

            var model = new CommonStatisticsModel();

            model.NumberOfOrders = _orderService.GetOrdersByAffiliateId(pageIndex: 0, pageSize: 1).TotalCount;

            model.NumberOfUsers = _userService.GetAllUsers(userRoleIds: new[] { _userService.GetUserRoleBySystemName(SystemRoleNames.Registered).Id },
                pageIndex: 0, pageSize: 1).TotalCount;

            model.NumberOfPendingAfterSales = _orderService.SearchAfterSales(0, 0,
                AfterSalesStatus.Pending,
                pageIndex: 0,
                pageSize: 1).TotalCount;

            model.NumberOfLowStockGoodss = _goodsService.GetLowStockGoodss(0, 1).TotalCount +
                                             _goodsService.GetLowStockGoodsCombinations(0, 1).TotalCount;

            return View(model);
        }
        
        #endregion
    }
}
